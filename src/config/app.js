require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
})

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('./cors')
const morgan = require('morgan')
const logger = require('./logger')
//const middlewares = require('./middlewares')

const prefix = '/api'

class AppController {
    constructor() {
        this.server = express()

        this.cors()
        this.middlewares()
        this.routes()
    }

    cors() {
        this.server.use(cors)
    }

    middlewares() {
        this.server.use(bodyParser.urlencoded({extended: true}))
        this.server.use(bodyParser.json())
        this.server.use(morgan('combined', { stream: logger.stream }))

        //middlewares for api routes
        //this.server.use(prefix, middlewares.verifyToken)
    }

    routes() {
        require('../routes/index')(this.server)
    }
}

module.exports = new AppController().server