const router = require('express').Router()
const categoryController = require('../controllers/category')
const route = '/api/category'

module.exports = (server) => {
    router.get('/', categoryController.all)
    router.get('/:id', categoryController.find)
    router.post('/', categoryController.create)
    router.put('/:id', categoryController.update)
    router.delete('/:id', categoryController.remove)

    server.use(route, router)
}