const { Product, Category } = require('../models')

class ProductController {
    async all(req, res) {
        try {
            const products = await Product.findAll({
                include: [
                    {
                    model: Category,
                    as: 'categories',
                    through: { attributes: [] },
                    },
                ]
            })        

            return res.json(products)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async find(req, res) {
        try {
            const { id } = req.params
            const product = await Product.findOne({
                where: {
                    id
                },
                include: [
                    {
                    model: Category,
                    as: 'categories',
                    through: { attributes: [] },
                    },
                ]            
            })

            return res.json(product)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async create(req, res) {
        try {
            const { categories, ...data } = req.body
	        const product = await Product.create(data)

            if (categories && categories.length > 0) {
                product.setCategories(categories)
            }

            return res.status(201).json(product)		
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async update(req, res) {
        try {
            const { id } = req.params                  
            const { categories, ...data } = req.body
            const product = await Product.findOne({
                where: { id }
            })
            
            product.update(data)
            
            if (categories && categories.length > 0) {
                product.setCategories(categories)        
            }

            return res.json(product)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async remove(req, res) {
        try {
            const { id } = req.params
            const product = await Product.findOne({
                where: { id }
            })
            
            product.destroy(id)

            return res.json(product)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }
}

module.exports = new ProductController()