'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    sku: DataTypes.STRING,
    price: DataTypes.FLOAT,
    description: DataTypes.STRING,
    amount: DataTypes.INTEGER
  }, {});
  Product.associate = function(models) {
    // associations can be defined here
    Product.belongsToMany(models.Category, { 
      through: 'ProductCategories',
      as: 'categories',
      foreignKey: 'product_id'
    });
  };
  return Product;
};