const fs = require('fs')
const csv = require('csv-parser')
const program = require('commander')
const package = require('../package.json')
const { Product } = require('../src/models')

const readCSV = path => {    
    fs.createReadStream(path)  
        .pipe(csv({separator: ';'}))
        .on('data', row => {            
            add(row)
        })
        .on('end', () => {            
            console.log('CSV file successfully processed')
        })
}

const add = async row => {
    try {            
        const { categories, ...data } = row
        const product = await Product.create(data)

        if (categories && categories.length > 0) {
            product.setCategories(categories)
        }
        console.log(product)
    } catch (err) {
        console.log({ err })
    }
}

program.version(package.version);

program
    .command('add [csv]')
    .description('Importa um arquivo csv')
    .action(filePath => {
        readCSV(filePath)        
    })

program.parse(process.argv)