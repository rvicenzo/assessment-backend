# Instalar dependencias
- Certifique-se de que a porta 3003 esteja liberada, ou altere a porta em src/loader.js(linha 3)
- Defina as credenciais do seu banco de dados local(mysql) no arquivo .env
- npm i
- sequelize db:migrate (requer npm install -g sequelize-cli)

#Gerenciar aplicação
- Na raiz do projeto: 
- npm run dev (desenvolvimento)
- npm run production (produção)
- * pm2 stop app (requer npm install pm2 -g)
- * pm2 restart

#Logs
- Na raiz do projeto, crie uma pasta chamada logs

#Testes
- Na raiz do projeto, execute: 
- npm run test

#Importar dados do arquivo CSV
- Na raiz do projeto, execute: 
- node cli/index add assets/import.csv
