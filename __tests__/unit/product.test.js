const { Product, Category } = require("../../src/models")

describe("Product", () => {
    it("should list all products", async () => {
        const products = await Product.findAll({
            include: [
                {
                model: Category,
                as: 'categories',
                through: { attributes: [] },
                },
            ]
        })

        expect.arrayContaining(products)        
    })
})